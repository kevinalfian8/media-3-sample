 package com.kevin.media3sample

import android.content.Context
import android.os.Handler
import androidx.media3.common.util.UnstableApi
import androidx.media3.exoplayer.DefaultRenderersFactory
import androidx.media3.exoplayer.Renderer
import androidx.media3.exoplayer.audio.AudioRendererEventListener
import androidx.media3.exoplayer.audio.AudioSink
import androidx.media3.exoplayer.mediacodec.MediaCodecSelector
import java.util.ArrayList

 @UnstableApi
class CustomRendererFactory(context: Context,private val audioTrackCount: Int): DefaultRenderersFactory(context) {

     override fun buildAudioRenderers(
         context: Context,
         extensionRendererMode: Int,
         mediaCodecSelector: MediaCodecSelector,
         enableDecoderFallback: Boolean,
         audioSink: AudioSink,
         eventHandler: Handler,
         eventListener: AudioRendererEventListener,
         out: ArrayList<Renderer>
     ) {

     }


}