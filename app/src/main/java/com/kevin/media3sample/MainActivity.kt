package com.kevin.media3sample

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.annotation.OptIn
import androidx.appcompat.app.AppCompatActivity
import androidx.media3.common.MediaItem
import androidx.media3.common.PlaybackParameters
import androidx.media3.common.Player
import androidx.media3.common.Tracks
import androidx.media3.common.util.UnstableApi
import androidx.media3.datasource.DefaultDataSource
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.exoplayer.source.MergingMediaSource
import androidx.media3.exoplayer.source.ProgressiveMediaSource
import androidx.media3.exoplayer.trackselection.DefaultTrackSelector
import androidx.media3.exoplayer.trackselection.DefaultTrackSelector.SelectionOverride
import kotlin.math.pow


class MainActivity : AppCompatActivity() {

    private lateinit var mTrackSelector: DefaultTrackSelector

    private lateinit var player: ExoPlayer
    private lateinit var player1: ExoPlayer
    private lateinit var player2: ExoPlayer
    private lateinit var player3: ExoPlayer

    private var isReady = false
    private var isReady1 = false
    private var isReady2 = false
    private var isReady3 = false

    private lateinit var playbackParameters: PlaybackParameters
    var currentPitch = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initPlayer()

        findViewById<Button>(R.id.btn_play_pause).setOnClickListener {
            if (isReady && isReady1 && isReady2 && isReady3) {
                if (player.isPlaying) {
                    player.pause()
                    player1.pause()
                    player2.pause()
                    player3.pause()
                } else {
                    player.play()
                    player1.play()
                    player2.play()
                    player3.play()
                }

            } else {
                Toast.makeText(this@MainActivity, "Not Ready", Toast.LENGTH_SHORT).show()
            }
        }

        findViewById<Button>(R.id.min_tempo).setOnClickListener {
            playbackParameters = PlaybackParameters(playbackParameters.speed - 0.1F, convertPitch(currentPitch))
            onChangePlaybackParam()
        }

        findViewById<Button>(R.id.plus_tempo).setOnClickListener {
            playbackParameters = PlaybackParameters(playbackParameters.speed + 0.1F, convertPitch(currentPitch))
            onChangePlaybackParam()
        }

        findViewById<Button>(R.id.min_transpose).setOnClickListener {
            currentPitch -= 1
            playbackParameters = PlaybackParameters(playbackParameters.speed, convertPitch(currentPitch))
            onChangePlaybackParam()
        }

        findViewById<Button>(R.id.plus_transpose).setOnClickListener {
            currentPitch += 1
            playbackParameters = PlaybackParameters(playbackParameters.speed, convertPitch(currentPitch))
            onChangePlaybackParam()

        }
    }

    private fun onChangePlaybackParam() {
        player.playbackParameters =
            playbackParameters
        player1.playbackParameters =
            playbackParameters
        player2.playbackParameters =
            playbackParameters
        player3.playbackParameters =
            playbackParameters
    }

    private fun convertPitch(pitch: Double): Float {
        val number = 2.0
        return number.pow(pitch / 12).toFloat()
    }

    @OptIn(UnstableApi::class)
    private fun initPlayer() {

        playbackParameters = PlaybackParameters(1.0F, convertPitch(currentPitch))
        mTrackSelector = DefaultTrackSelector(this)
        player = ExoPlayer.Builder(this)
            .build()

        player1 = ExoPlayer.Builder(this)
            .build()

        player2 = ExoPlayer.Builder(this)
            .build()

        player3 = ExoPlayer.Builder(this)
            .build()


        val sourceVocal =
            createMediaSource("https://storage.googleapis.com/song_sounds/261/261-vocal.mp3")
        val sourceGuitar =
            createMediaSource("https://storage.googleapis.com/song_sounds/261/261-guitar.mp3")
        val sourceBass =
            createMediaSource("https://storage.googleapis.com/song_sounds/261/261-bass.mp3")
        val sourceDrum =
            createMediaSource("https://storage.googleapis.com/song_sounds/261/261-drum.mp3")

        player.setMediaSource(sourceVocal)
        player.playbackParameters = playbackParameters
        player.prepare()

        player1.setMediaSource(sourceGuitar)
        player1.playbackParameters = playbackParameters
        player1.prepare()

        player2.setMediaSource(sourceBass)
        player2.playbackParameters = playbackParameters
        player2.prepare()

        player3.setMediaSource(sourceDrum)
        player3.playbackParameters = playbackParameters
        player3.prepare()

        player.addListener(object : Player.Listener {

            override fun onPlaybackStateChanged(playbackState: Int) {
                when (playbackState) {
                    Player.STATE_READY -> {
                        isReady = true

                        Toast.makeText(this@MainActivity, "1 Ready", Toast.LENGTH_SHORT).show()
                    }
                }
                super.onPlaybackStateChanged(playbackState)
            }
        })

        player1.addListener(object : Player.Listener {
            override fun onPlaybackStateChanged(playbackState: Int) {
                when (playbackState) {
                    Player.STATE_READY -> {
                        isReady1 = true

                        Toast.makeText(this@MainActivity, "2 Ready", Toast.LENGTH_SHORT).show()
                    }
                }
                super.onPlaybackStateChanged(playbackState)
            }
        })

        player2.addListener(object : Player.Listener {
            override fun onPlaybackStateChanged(playbackState: Int) {
                when (playbackState) {
                    Player.STATE_READY -> {
                        isReady2 = true

                        Toast.makeText(this@MainActivity, "3 Ready", Toast.LENGTH_SHORT).show()
                    }
                }
                super.onPlaybackStateChanged(playbackState)
            }
        })

        player3.addListener(object : Player.Listener {
            override fun onPlaybackStateChanged(playbackState: Int) {
                when (playbackState) {
                    Player.STATE_READY -> {
                        isReady3 = true

                        Toast.makeText(this@MainActivity, "3 Ready", Toast.LENGTH_SHORT).show()
                    }
                }
                super.onPlaybackStateChanged(playbackState)
            }
        })
    }

    @OptIn(UnstableApi::class)
    fun setPlayTrack(original: Boolean) {
        val trackInfo = mTrackSelector.currentMappedTrackInfo
        if (trackInfo != null) {
            if (original) {
                mTrackSelector.setParameters(
                    mTrackSelector.buildUponParameters()
                        .setSelectionOverride(
                            1,
                            trackInfo.getTrackGroups(1),
                            SelectionOverride(1, 0)
                        )
                )
            } else {
                mTrackSelector.setParameters(
                    mTrackSelector.buildUponParameters()
                        .setSelectionOverride(
                            1,
                            trackInfo.getTrackGroups(1),
                            SelectionOverride(0, 0)
                        )
                )
            }
        }
    }

    @OptIn(UnstableApi::class)
    private fun createMediaSource(url: String) =
        ProgressiveMediaSource.Factory(DefaultDataSource.Factory(this))
            .createMediaSource(MediaItem.fromUri(url))
}